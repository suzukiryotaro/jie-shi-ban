package bbs.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bbs.beans.User;
import bbs.service.UserService;

@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	HttpSession session = request.getSession();

    	User user = (User) session.getAttribute("loginUser");

			if (user.getDepartment() == 1) {
				List<User> users = new UserService().getUser();
		        request.setAttribute("users", users);
		        request.getRequestDispatcher("./edit.jsp").forward(request, response);
//				response.sendRedirect("./edit");
			} else {
				List<String> messages = new ArrayList<String>();
				messages.add("権限がないためアクセスできません");
				session.setAttribute("errorMessages", messages);
				response.sendRedirect("home");
			}

		}
    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

            User state = new User();
            state.setState(Integer.parseInt(request.getParameter("state")));
            state.setUser_id(Integer.parseInt(request.getParameter("user_id")));

            new UserService().state(state);
            response.sendRedirect("edit");

    	}
	}

