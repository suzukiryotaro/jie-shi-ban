package bbs.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bbs.beans.User;
import bbs.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        UserService service = new UserService();

        request.setAttribute("branches", service.getBranches());
        request.setAttribute("departments", service.getDepartments());

        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<String> messages = new ArrayList<String>();

        HttpSession session = request.getSession();
        if (isValid(request, messages) == true) {

            User user = new User();

            user.setId(request.getParameter("id"));
            user.setName(request.getParameter("name"));
            user.setPassword(request.getParameter("password"));
            user.setBranch(Integer.parseInt(request.getParameter("branch")));
            user.setDepartment(Integer.parseInt(request.getParameter("department")));

            new UserService().register(user);

            response.sendRedirect("login");
        } else {
            session.setAttribute("errorMessages", messages);
            response.sendRedirect("signup");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {
        String id = request.getParameter("id");
        String password = request.getParameter("password");
        String name = request.getParameter("name");

        if (StringUtils.isEmpty(name) == true) {
            messages.add("名前を入力してください");
        }
        if (StringUtils.isEmpty(id) == true) {
            messages.add("ログインIDを入力してください");
        }
        if (StringUtils.isEmpty(password) == true) {
            messages.add("パスワードを入力してください");
        }

        // TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}