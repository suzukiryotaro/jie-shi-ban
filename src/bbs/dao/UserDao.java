package bbs.dao;

import static bbs.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import bbs.beans.Branch;
import bbs.beans.Department;
import bbs.beans.User;
import bbs.exception.NoRowsUpdatedRuntimeException;
import bbs.exception.SQLRuntimeException;

public class UserDao {

	public void insert(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
			sql.append("id");
			sql.append(", name ");
			sql.append(", branch ");
			sql.append(", department ");
			sql.append(", password ");
			sql.append(", state ");
			sql.append(", created_date ");
			sql.append(", updated_date ");
			sql.append(") VALUES (");
			sql.append("?"); // id
			sql.append(", ? "); // name
			sql.append(", ? "); // branch
			sql.append(", ? "); // department
			sql.append(", ? "); // password
			sql.append(", ? "); // state
			sql.append(", CURRENT_TIMESTAMP "); // created_date
			sql.append(", CURRENT_TIMESTAMP "); // updated_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getId());
			ps.setString(2, user.getName());
			ps.setInt(3, user.getBranch());
			ps.setInt(4, user.getDepartment());
			ps.setString(5, user.getPassword());
			ps.setInt(6, user.getState());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public User getUser(Connection connection, String id,
			String password) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE id = ? AND password = ? ";

			ps = connection.prepareStatement(sql);
			ps.setString(1, id);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 = userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<User> toUserList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				int user_id = rs.getInt("user_id");
				String id = rs.getString("id");
				String name = rs.getString("name");
				int branch = rs.getInt("branch");
				int department = rs.getInt("department");
				String password = rs.getString("password");
				int state = rs.getInt("state");
				Timestamp createdDate = rs.getTimestamp("created_date");
				Timestamp updatedDate = rs.getTimestamp("updated_date");

				User user = new User();
				user.setUser_id(user_id);
				user.setId(id);
				user.setName(name);
				user.setBranch(branch);
				user.setDepartment(department);
				user.setPassword(password);
				user.setState(state);
				user.setCreatedDate(createdDate);
				user.setUpdatedDate(updatedDate);

				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public User getUser(Connection connection, String i) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE id = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, i);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void update(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append("  id = ?");
			sql.append(", name = ?");
			sql.append(", branch = ?");
			sql.append(", department = ?");
			sql.append(", password = ?");
			sql.append(", state = ?");
			sql.append(", updated_date = CURRENT_TIMESTAMP");
			sql.append(" WHERE");
			sql.append(" user_id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getId());
			ps.setString(2, user.getName());
			ps.setInt(3, user.getBranch());
			ps.setInt(4, user.getDepartment());
			ps.setString(5, user.getPassword());
			ps.setInt(6, user.getState());
			ps.setInt(7, user.getUser_id());

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

	public List<Branch> getBranches(Connection conn) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM branches";
			ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			return toBranchList(rs);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Branch> toBranchList(ResultSet rs) throws SQLException {
		List<Branch> ret = new ArrayList<>();

		while (rs.next()) {
			int branch_id = rs.getInt("branch_id");
			String branch_name = rs.getString("branch_name");

			Branch branch = new Branch();
			branch.setBranch_id(branch_id);
			branch.setBranch_name(branch_name);

			ret.add(branch);
		}

		return ret;
	}

	public List<Department> getDepartments(Connection conn) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM departments";
			ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			return toDepartmentList(rs);
		} catch (SQLException e) {

			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Department> toDepartmentList(ResultSet rs) throws SQLException {
		List<Department> ret = new ArrayList<>();

		while (rs.next()) {
			int department_id = rs.getInt("department_id");
			String department_name = rs.getString("department_name");

			Department department = new Department();
			department.setDepartment_id(department_id);
			department.setDepartment_name(department_name);

			ret.add(department);
		}

		return ret;
	}

	public User getUsers(Connection connection, int user_id) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE user_id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, user_id);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("1 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	public void state(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append(" state = ?");
			sql.append(" WHERE");
			sql.append(" user_id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, user.getState());
			ps.setInt(2, user.getUser_id());

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
		public void check(Connection connection, int department, User user) {

			PreparedStatement ps = null;
			try {
				String sql = "SELECT * FROM users WHERE department = ? ";

				ps = connection.prepareStatement(sql);
				ps.setInt(1, department);

				int count = ps.executeUpdate();
				if (count == 0) {
					throw new NoRowsUpdatedRuntimeException();
				}
			} catch (SQLException e) {
				throw new SQLRuntimeException(e);
			} finally {
				close(ps);
			}
		}
}