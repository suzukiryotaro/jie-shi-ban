<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー編集</title>
</head>
<body>
	<h3>【登録アカウント一覧】</h3>
	<table border="1">
		<thead>
			<tr>
				<th>名 前</th>
				<th>支 店</th>
				<th>部署・役職</th>
				<th>状態</th>
				<th>編集</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${users}" var="user">
				<tr>
					<td><div class="name">
							<c:out value="${user.name}" />
						</div></td>

					<td><div class="branch_name">
							<c:out value="${user.branch_name}" />
						</div></td>

					<td><div class="department_name">
							<c:out value="${user.department_name}" />
						</div></td>

					<td>

					<c:if test="${user.state == 0}">
							<form action="edit" method="post">
								<button name="state" type="submit" value="1">
									<font color="red"><strong>停止</strong></font>
								</button>
								<input name="user_id" type="hidden" value="${user.user_id }">
							</form>

						</c:if> <c:if test="${user.state == 1}">
							<form action="edit" method="post">
								<button name="state" type="submit" value="0">
									<font color="blue"><strong>再開</strong></font>
								</button>
								<input name="user_id" type="hidden" value="${user.user_id }">

							</form>
						</c:if></td>

					<td><a href="settings?user_id=${user.user_id}">編集</a></td>
				</tr>
			</c:forEach>
	</table>
	<a href="signup">新規ユーザー登録</a>
	<br />
	<a href="logout">ログアウト</a>
	<br />
	<a href="home">戻る</a>
	<br />
</body>
</html>