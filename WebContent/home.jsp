<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ホーム</title>
</head>
<body>
	<span style="border: 1px solid">社内掲示板ホーム</span>
	<h4>【メニュー】</h4>
	<div class="main-contents">
		<a href="newpost.jsp">新規投稿</a><br /> <a
			href="edit?department=${loginUser.department}">ユーザー管理</a><br /> <a
			href="logout">ログアウト</a><br />
	</div>

	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="message">
					<li><c:out value="${message}" />
				</c:forEach>
			</ul>
		</div>
		<c:remove var="errorMessages" scope="session" />
	</c:if>

	<form action="home" method="get">
	<br>【カテゴリ検索】<br> <input type="text" name="search" size=20 />
		 <input type="submit" value="検索" />
	</form>

	<br>【日付検索】
	<br>
	<input type="date"></input> ～
	<input type="date"></input>
	<br>
	<button name="delete_id" type="submit" value="1">検索</button>

	<h4>【投稿一覧】</h4>
	================================
	<div class="messages">
		<c:forEach items="${messages}" var="message">
			<c:if test="${message.deletem_id == 0}">
				<div class="message">
					<div class="name">
						《投稿者》 <br /> <span class="name"><c:out
								value="${message.name}" /></span>
					</div>
					《件名》
					<div class="subject">
						<c:out value="${message.subject}" />
					</div>
					《カテゴリー》
					<div class="category">
						<c:out value="${message.category}" />
					</div>
					《本文》
					<div class="text">
						<c:out value="${message.text}" />
					</div>
					<div class="date">
						<fmt:formatDate value="${message.created_date}"
							pattern="yyyy/MM/dd HH:mm:ss" />
					</div>
					<form action="delete" method="post">
						<c:if test="${message.id == loginUser.user_id}">
							<td><button name="deletem_id" type="submit" value="1">
									削除<br /> <input name="message_id" type="hidden"
										value="${message.message_id }">
								</button></td>
						</c:if>
					</form>
					<div class="form-area">
						<form action="newComment" method="post">
							コメント <br />
							<textarea name="text" cols="25" rows="5" class="tweet-box"></textarea>
							<br /> <input type="submit" value="投稿"> <input
								name="post_Id" type="hidden" value="${message.message_id }">
						</form>
					</div>
				</div>

				<span style="border: 1px solid">コメント一覧</span>

				<div class="comments">
					<c:forEach items="${comments}" var="comment">
						<c:if test="${comment.delete_id == 0}">
							<c:if test="${comment.post_id == message.message_id}">
								<div class="comment">
									<div class="name">
										《名前》 <br /> <span class="name"><c:out
												value="${comment.name}" /></span>
									</div>
									《コメント》
									<div class="text">
										<c:out value="${comment.text}" />
									</div>
									<div class="date">
										<fmt:formatDate value="${comment.created_date}"
											pattern="yyyy/MM/dd HH:mm:ss" />

										<form action="home" method="post">
											<c:if test="${comment.id == loginUser.user_id}">
												<td><button name="delete_id" type="submit" value="1">
														削除<br /> <input name="comment_id" type="hidden"
															value="${comment.comment_id }">
													</button></td>
											</c:if>
										</form>
									</div>
								</div>
							</c:if>
						</c:if>
					</c:forEach>
					<br />================================
				</div>
			</c:if>
		</c:forEach>
	</div>
</body>
</html>