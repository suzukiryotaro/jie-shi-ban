package bbs.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import bbs.beans.Comment;
import bbs.beans.User;
import bbs.beans.UserComment;
import bbs.beans.UserMessage;
import bbs.service.CommentService;
import bbs.service.MessageService;

@WebServlet(urlPatterns = { "/home" })
public class HomeServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        User user = (User) request.getSession().getAttribute("loginUser");
        boolean isShowMessageForm;
        if (user != null) {
            isShowMessageForm = true;
        } else {
            isShowMessageForm = false;
        }

        String category = request.getParameter("search");

        List<UserComment>comments = new CommentService().getComment();
        request.setAttribute("comments", comments);

        if(StringUtils.isEmpty(category)) {
        List<UserMessage> messages = new MessageService().getMessage();
        request.setAttribute("messages", messages);
        request.setAttribute("isShowMessageForm", isShowMessageForm);

        }else {
        List<UserMessage> message2 = new MessageService().category(category);
        request.setAttribute("messages", message2);
    }

    request.getRequestDispatcher("home.jsp").forward(request, response);
    }

@Override
protected void doPost(HttpServletRequest request,
        HttpServletResponse response) throws IOException, ServletException {

        Comment deletem = new Comment();
        deletem.setDelete_id(Integer.parseInt(request.getParameter("delete_id")));
        deletem.setComment_id(Integer.parseInt(request.getParameter("comment_id")));

        new CommentService().deletem(deletem);

        response.sendRedirect("home");
	}
}

//protected void doPost1(HttpServletRequest request,
//       HttpServletResponse response) throws IOException, ServletException {
//
//	    System.out.println(request.getParameter("deletem_id"));
//	    System.out.println(request.getParameter("message_id"));
//
//        Message delete2 = new Message();
//        delete2.setDeletem_id(Integer.parseInt(request.getParameter("deletem_id")));
//        delete2.setMessage_id(Integer.parseInt(request.getParameter("message_id")));
//
//        new MessageService().delete2(delete2);
//
//        response.sendRedirect("home");


