package bbs.dao;

import static bbs.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import bbs.beans.Comment;
import bbs.beans.UserComment;
import bbs.exception.NoRowsUpdatedRuntimeException;
import bbs.exception.SQLRuntimeException;

public class UserCommentDao {

    public List<UserComment> getUserComments(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("comments.text as text, ");
            sql.append("comments.id as id, ");
            sql.append("comments.post_id as post_id, ");
            sql.append("comments.created_date as created_date, ");
            sql.append("comments.delete_id as delete_id, ");
            sql.append("comments.comment_id as comment_id, ");
            sql.append("users.user_id as user_id,");
            sql.append("users.name as name ");
            sql.append("FROM comments ");
            sql.append("INNER JOIN users ");
            sql.append("ON comments.id = users.user_id ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserComment> ret = toUserCommentList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserComment> toUserCommentList(ResultSet rs)
            throws SQLException {

        List<UserComment> ret = new ArrayList<UserComment>();
        try {
            while (rs.next()) {

                String text = rs.getString("text");
                String id = rs.getString("id");
                int user_id = rs.getInt("user_id");
            	String name = rs.getString("name");
            	int post_id = rs.getInt("post_id");
            	int delete_id = rs.getInt("delete_id");
            	int comment_id = rs.getInt("comment_id");
                Timestamp createdDate = rs.getTimestamp("created_date");

                UserComment comment = new UserComment();
                comment.setText(text);
                comment.setId(id);
                comment.setUser_id(user_id);
                comment.setName(name);
                comment.setPost_id(post_id);
                comment.setDelete_id(delete_id);
                comment.setComment_id(comment_id);
                comment.setCreated_date(createdDate);


                ret.add(comment);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
    public void deletem(Connection connection, Comment comment) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE comments SET");
			sql.append(" delete_id = ?");
			sql.append(" WHERE");
			sql.append(" comment_id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, comment.getDelete_id());
			ps.setInt(2, comment.getComment_id());

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}
}