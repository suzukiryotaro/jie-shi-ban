<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー登録</title>
</head>
<body>
	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<form action="signup" method="post">
			<br /> <label for="name">名前</label> <input name="name" id="name" /><br />
			<label for="id">ログインID</label> <input name="id" id="id" /> <br />
			<label for="password">パスワード</label> <input name="password"
				type="password" id="password" /> <br />
			支店<select name="branch">
				<c:forEach var="branch" items="${branches}">
					<option value="${branch.branch_id}">${branch.branch_name}</option>
				</c:forEach>
			</select> <br>
			役職<select name="department">
				<c:forEach var="department" items="${departments}">
					<option value="${department.department_id}">${department.department_name}</option>
				</c:forEach>
			</select> <br>
			<input type ="submit" value="登録" /> <br />
			<a href="home">戻る</a>
		</form>
		<div class="copyright">Copyright(c)Your Name</div>
	</div>
</body>
</html>