package bbs.dao;

import static bbs.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import bbs.beans.Message;
import bbs.exception.SQLRuntimeException;

public class MessageDao {

    public void insert(Connection connection, Message message) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO messages ( ");
            sql.append("text");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(", subject");
            sql.append(", category");
            sql.append(", id");
            sql.append(") VALUES (");
            sql.append("?");
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(", ?");
            sql.append(", ?");
            sql.append(", ?");
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, message.getText());
            ps.setString(2, message.getSubject());
            ps.setString(3, message.getCategory());
            ps.setInt(4, message.getId());


            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

}