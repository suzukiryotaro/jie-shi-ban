package bbs.dao;

import static bbs.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bbs.beans.User;
import bbs.exception.SQLRuntimeException;

public class EditDao {

    public List<User> getUser(Connection connection) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("users.name as name, ");
            sql.append("users.user_id as user_id, ");
            sql.append("users.branch as branch, ");
            sql.append("users.department as department, ");
            sql.append("users.id as id, ");
            sql.append("users.password as password, ");
            sql.append("branches.branch_id as branch_id, ");
            sql.append("branches.branch_name as branch_name, ");
            sql.append("departments.department_id as department_id, ");
            sql.append("departments.department_name as department_name, ");
            sql.append("users.state as state ");
            sql.append("FROM users ");
            sql.append("LEFT JOIN branches ");
            sql.append("ON branches.branch_id = users.branch ");
            sql.append("LEFT JOIN departments ");
            sql.append("ON departments.department_id = users.department ");


            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<User> ret = toUserList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<User> toUserList(ResultSet rs)
            throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
            	String name = rs.getString("name");
                String branch_name = rs.getString("branch_name");
                String department_name = rs.getString("department_name");
                int user_id = rs.getInt("user_id");
                String id = rs.getString("id");
                String password = rs.getString("password");
                int branch_id = rs.getInt("branch_id");
            	int department_id =rs.getInt("department_id");
            	int state = rs.getInt("state");

                User user = new User();
                user.setName(name);
                user.setBranch_name(branch_name);
                user.setDepartment_name(department_name);
                user.setUser_id(user_id);
                user.setId(id);
                user.setPassword(password);
                user.setBranch_id(branch_id);
                user.setDepartment_id(department_id);
                user.setState(state);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
}