package bbs.dao;

import static bbs.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import bbs.beans.Message;
import bbs.beans.UserMessage;
import bbs.exception.NoRowsUpdatedRuntimeException;
import bbs.exception.SQLRuntimeException;

public class UserMessageDao {

    public List<UserMessage> getUserMessages(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("messages.subject as subject, ");
            sql.append("messages.text as text, ");
            sql.append("messages.category as category, ");
            sql.append("messages.id as id, ");
            sql.append("messages.message_id as message_id, ");
            sql.append("messages.deletem_id as deletem_id, ");
            sql.append("users.user_id as user_id, ");
            sql.append("users.name as name, ");
            sql.append("messages.created_date as created_date ");
            sql.append("FROM messages ");
            sql.append("INNER JOIN users ");
            sql.append("ON messages.id = users.user_id ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserMessage> ret = toUserMessageList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserMessage> toUserMessageList(ResultSet rs)
            throws SQLException {

        List<UserMessage> ret = new ArrayList<UserMessage>();
        try {
            while (rs.next()) {
            	String subject = rs.getString("subject");
                String text = rs.getString("text");
                String category = rs.getString("category");
                String id = rs.getString("id");
                int user_id = rs.getInt("user_id");
            	String name = rs.getString("name");
            	int message_id =rs.getInt("message_id");
            	int deletem_id =rs.getInt("deletem_id");
                Timestamp createdDate = rs.getTimestamp("created_date");

                UserMessage message = new UserMessage();
                message.setName(name);
                message.setSubject(subject);
                message.setText(text);
                message.setCategory(category);
                message.setId(id);
                message.setUser_id(user_id);
                message.setMessage_id(message_id);
                message.setDeletem_id(deletem_id);
                message.setCreated_date(createdDate);

                ret.add(message);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
    public void delete2(Connection connection, Message message) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE messages SET");
			sql.append(" deletem_id = ?");
			sql.append(" WHERE");
			sql.append(" message_id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, message.getDeletem_id());
			ps.setInt(2, message.getMessage_id());

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}
    public  List<UserMessage> category(Connection connection, String category) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("messages.subject as subject, ");
            sql.append("messages.text as text, ");
            sql.append("messages.category as category, ");
            sql.append("messages.id as id, ");
            sql.append("messages.message_id as message_id, ");
            sql.append("messages.deletem_id as deletem_id, ");
            sql.append("users.user_id as user_id, ");
            sql.append("users.name as name, ");
            sql.append("messages.created_date as created_date ");
            sql.append("FROM messages ");
            sql.append("INNER JOIN users ");
            sql.append("ON messages.id = users.user_id ");
            sql.append("WHERE category LIKE ?");
//            sql.append("ORDER BY created_date DESC limit ");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1,"%" + category + "%");

            ResultSet rs = ps.executeQuery();
            List<UserMessage> ret = toUserMessageList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
}
