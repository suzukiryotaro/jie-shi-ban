package bbs.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bbs.beans.Message;
import bbs.service.MessageService;

@WebServlet(urlPatterns = { "/delete" })
public class DeleteCommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
protected void doPost(HttpServletRequest request,
       HttpServletResponse response) throws IOException, ServletException {

	    System.out.println(request.getParameter("deletem_id"));
	    System.out.println(request.getParameter("message_id"));

        Message delete2 = new Message();
        delete2.setDeletem_id(Integer.parseInt(request.getParameter("deletem_id")));
        delete2.setMessage_id(Integer.parseInt(request.getParameter("message_id")));

        new MessageService().delete2(delete2);

        response.sendRedirect("home");

	}
}
