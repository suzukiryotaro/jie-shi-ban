package bbs.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bbs.beans.Comment;
import bbs.beans.User;
import bbs.service.CommentService;

@WebServlet(urlPatterns = { "/newComment" })
public class NewCommentServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("home.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();

        List<String> comment = new ArrayList<String>();

        if (isValid(request, comment) == true) {

            User user = (User) session.getAttribute("loginUser");

            Comment comments = new Comment();
            comments.setText(request.getParameter("text"));
            comments.setId(user.getUser_id());
            comments.setPost_id(Integer.parseInt(request.getParameter("post_Id")));

            new CommentService().register(comments, comments);

            response.sendRedirect("home");
        } else {
            session.setAttribute("errorMessages", comment);
            response.sendRedirect("home");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> comment) {

        String text = request.getParameter("text");

        if (StringUtils.isEmpty(text) == true) {
            comment.add("本文を入力してください");
        }
        if (500 < text.length()) {
            comment.add("500文字以下で入力してください");
        }
        if (comment.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}