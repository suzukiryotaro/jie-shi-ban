<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ホーム</title>
</head>
<body>
	<a href="signup">新規ユーザー登録</a><br/>
	<a href="edit">ユーザー編集</a><br/>
	<a href="logout">ログアウト</a><br/>
	<a href="home">戻る</a><br/>
</body>
</html>