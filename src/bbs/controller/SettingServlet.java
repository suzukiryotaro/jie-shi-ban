package bbs.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bbs.beans.User;
import bbs.exception.NoRowsUpdatedRuntimeException;
import bbs.service.UserService;

@WebServlet(urlPatterns = { "/settings" })
public class SettingServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        User editUser = new UserService().getUsers(Integer.parseInt(request.getParameter("user_id")));
        request.setAttribute("editUser", editUser);

        UserService service = new UserService();

        request.setAttribute("branches", service.getBranches());
        request.setAttribute("departments", service.getDepartments());

        request.getRequestDispatcher("settings.jsp").forward(request, response);

	}

@Override
protected void doPost(HttpServletRequest request,
        HttpServletResponse response) throws ServletException, IOException {

    List<String> messages = new ArrayList<String>();
    HttpSession session = request.getSession();
    User editUser = getEditUser(request);

    if (isValid(request, messages) == true) {

        try {
            new UserService().update(editUser);
        } catch (NoRowsUpdatedRuntimeException e) {
            messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
            session.setAttribute("errorMessages", messages);
            request.setAttribute("editUser", editUser);
            request.getRequestDispatcher("settings.jsp").forward(request, response);
            return;
        }

//        session.setAttribute("editUser", editUser);

        response.sendRedirect("edit");
    } else {
        session.setAttribute("errorMessages", messages);
        request.setAttribute("editUser", editUser);
        request.getRequestDispatcher("settings.jsp").forward(request, response);
    }
}

private User getEditUser(HttpServletRequest request)
        throws IOException, ServletException {

    UserService service = new UserService();

    request.setAttribute("branches", service.getBranches());
    request.setAttribute("departments", service.getDepartments());

    User editUser = new User();
    editUser.setUser_id(Integer.parseInt(request.getParameter("user_id")));
    editUser.setName(request.getParameter("name"));
    editUser.setId(request.getParameter("id"));
    editUser.setBranch(Integer.parseInt(request.getParameter("branch")));
    editUser.setDepartment(Integer.parseInt(request.getParameter("department")));
    editUser.setPassword(request.getParameter("password"));
    return editUser;
}


private boolean isValid(HttpServletRequest request, List<String> messages) {

    String id = request.getParameter("id");
    String password = request.getParameter("password");

    if (StringUtils.isEmpty(id) == true) {
        messages.add("idを入力してください");
    }
    if (StringUtils.isEmpty(password) == true) {
        messages.add("パスワードを入力してください");
    }
    // TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
    if (messages.size() == 0) {
        return true;
    } else {
        return false;
    }
}
}