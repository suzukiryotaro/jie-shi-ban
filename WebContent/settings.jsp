<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ユーザー編集</title>
        <link href="css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="main-contents">

            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>

            <form action="settings" method="post"><br />
				<input name="user_id" value="${editUser.user_id}" id="user_id" type="hidden"/>
                <label for="name">名前</label>
                <input name="name" value="${editUser.name}" id="name"/><br />

				<label for="id">ID</label>
                <input name="id" value="${editUser.id}" id="id"/><br />

                <label for="password">パスワード</label>
                <input name="password" type="password" id="password"/> <br />

                支店
                <select name="branch">
				<c:forEach var="branch" items="${branches}">
				<option value="${branch.branch_id}">${branch.branch_name}</option>
				</c:forEach>
				</select>

				役職
                <select name="department">
				<c:forEach var="department" items="${departments}">
				<option value="${department.department_id}">${department.department_name}</option>
				</c:forEach>
				</select> <br>

                <input type="submit" value="登録" /> <br />
                <a href="edit">戻る</a>
            </form>
            <div class="copyright"> Copyright(c)Your Name</div>
        </div>
    </body>
</html>