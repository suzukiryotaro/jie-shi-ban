<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ホーム</title>
</head>
<body>
<div class="form-area">
        <form action="newMessage" method="post">
        	<br />件名
            <textarea name="subject" cols="40" rows="1" class="tweet-box"></textarea>
            カテゴリー
            <textarea name="category" cols="20" rows="1" class="tweet-box"></textarea>
            <br />本文
            <textarea name="text" cols="80" rows="5" class="tweet-box"></textarea>
            <br />
            <input type="submit" value="投稿">
        </form>
			<a href="home">戻る</a>
			<a href="logout">ログアウト</a>
</div>
</body>
</html>